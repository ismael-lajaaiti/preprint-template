\documentclass[10pt,letterpaper]{article}
\usepackage[top=0.85in,left=0.45in,footskip=0.75in,marginparwidth=2.3in]{geometry}

% use Unicode characters - try changing the option if you run into troubles with special characters (e.g. umlauts)
\usepackage[utf8]{inputenc}

\usepackage{mathpazo} % use palatino
\usepackage{fourier} % non-default font

% hyperref makes references clicky. use \url{www.example.com} or \href{www.example.com}{description} to add a clicky url
\usepackage[hidelinks]{hyperref}
\usepackage{nameref}

\usepackage{lipsum} % to generate filler text

% line numbers
% \usepackage[left]{lineno}

% improves typesetting in LaTeX
\usepackage{microtype}
\DisableLigatures[f]{encoding = *, family = * }

% text layout - change as needed
% \raggedright
\setlength{\parindent}{0.5cm}
\textwidth 5.25in
\textheight 8.75in

% Remove % for double line spacing
%\usepackage{setspace}
%\doublespacing

% use adjustwidth environment to exceed text width (see examples in text)
\usepackage{changepage}

% adjust caption style
\usepackage[aboveskip=1pt,labelfont=bf,labelsep=period,singlelinecheck=off]{caption}

% remove brackets from references
% \makeatletter
% \renewcommand{\@biblabel}[1]{\quad#1.}
% \makeatother

% headrule, footrule and page numbers
\usepackage{lastpage,fancyhdr,graphicx}
\usepackage{epstopdf}
% \pagestyle{myheadings}
\pagestyle{fancy}
\fancyhf{}
\rfoot{\thepage~of~\pageref{LastPage}}
\lfoot{\href{https://creativecommons.org/licenses/by-nc-sa/4.0/}
	{\color{blue} CC-BY-NC-SA 4.0}}
\renewcommand{\footrule}{\hrule height 0pt \vspace{2mm}}
\renewcommand{\headrulewidth}{0pt}
% \fancyheadoffset[L]{2.25in}
% \fancyfootoffset[L]{2.25in}

% use \textcolor{color}{text} for colored text (e.g. highlight to-do areas)
\usepackage{color}

% define custom colors (this one is for figure captions)
\definecolor{Gray}{gray}{.7}

\newcommand{\drawhline}{{\color{Gray}\noindent\rule{19.5cm}{0.1pt}}}

% this is required to include graphics
\usepackage{graphicx}

% use if you want to put caption to the side of the figure - see example in text
\usepackage{sidecap}

% use for have text wrap around figures
\usepackage{wrapfig}
\usepackage[pscoord]{eso-pic}
\usepackage[fulladjust]{marginnote}

% Journal bibliography style file (.bst) can found at:
% https://github.com/seananderson/bst
% Here we will the Methods in Ecology and Evolution file (`mee.bst`).
\usepackage[round]{natbib}
\bibliographystyle{mee.bst}

% document begins here
\begin{document}
\vspace*{0.2in}

% title goes here:
\begin{flushleft}
	{\Huge
		\textbf\newline{Manuscript title}
	}
	\vspace{0.5cm}
	\newline
	% authors go here with the ORCID link:
	\\
	\href{https://orcid.org/0000-0001-6138-8895}{\color{blue}{Ismaël Lajaaiti}}
	\textsuperscript{1},
	\href{https://orcid.org/0000-0001-9373-4051}
	{\color{blue}Jean-François Arnoldi}\textsuperscript{2},
	\href{https://orcid.org/0000-0001-6138-8895}{\color{blue}{Sonia Kéfi}}
	\textsuperscript{1}
	\\
	\marginnote{\raggedleft \today}
	\bigskip
	\textsuperscript{1} Institut des Sciences et de l'\'Evolution de Montpellier,
	Montpellier, France;
	\\
	\textsuperscript{2} Station Station d’Écologie Théorique et Expérimentale,
	Moulis, France.\\
	\bigskip
	\textbf{Correspondence to:}\\
	Corresponding Author --- \verb!corresponding@author.mail!

\end{flushleft}

\vspace{0.5cm} % breath
\drawhline

\section*{Abstract}

\marginnote{ % Keywords go here
	\raggedleft
	~\\
	\textbf{Keywords}\\
	ecological networks\\
	stability \\
	non-trophic interactions\\
}

Fitting multiple figures into very tight manuscripts while keeping it pleasant to read
is challenging.
Therefore figures are often simply attached to the very end of a manuscript file.
While easier for the authors, this practice is inconvenient for readers.
This \LaTeX template shows how to generate a compiled PDF with figures
embedded into the text. It provides several examples of how to embed figures or
tables directly into the text thus giving you a range of options from which
you should choose the one best suited for your manuscript. Check out Schlegel
et al., (2016) as example of use \citep{kefi2012}.

\vspace{.5cm} % breath
\drawhline
\vspace{0.8cm} % breath
% now start line numbers
% \linenumbers


% the * after section prevents numbering
\section*{Introduction}
In the introduction you will see an example of text wrapping around the figure with
a figure caption on the margin (Fig. \ref{fig2}).
This is done by combining the \verb!wrapfigure! with % avoid blank space here
the \verb!marginnote! environment.
Please note that in this case the figure (\verb!wrapfigure!) and the figure caption
(\verb!marginpar!) have to be separated as you can tell from the code.
The \verb!wrapfigure! environment can be a bit tricky when it comes to text formatting.
Thus some general hints: (a) try to avoid line breaks in the code as
this may result in weird formatting around the figure, (b) the figure should
not span multiple headlines (sections) and (c) if you encounter problems with the
line break right after the \verb!wrapfigure! try using \verb!\mbox{}!
to prevent premature line breaks (\mbox{see example in code}).
As stated in the figure caption, setting up figures this way requires a
bit more manual adjustments but it makes figures blend in nicely without
interrupting flow of text.

\section*{Materials and Methods}

In this section you will find an example of a table using the \verb!table!
plus the \verb!adjustwidth! environment and should give you a minimal
example for tables in \LaTeX.

\paragraph{Paragraph}
Instead of adding more and more subsections you can use the \verb!\paragraph{}!
command to give structure to your manuscript.

\subsection*{Formulas}
For mathematical formulas you should use the math environment. See this example:

\begin{center}
	$f(A_{ik},A_{jk}) = min(A_{ik},A_{jk}) - C_{1} max(A_{ik},A_{jk}) e^{-C_{2}min(A_{ik},A_{jk})}$
\end{center}

\section*{Results}
\subsection*{Standard floating figures.}
Figure \ref{fig2} is wrapped into a standard floating environment.
That means that \LaTeX will determine the exact placement of the figure.
Even though you can state preferences (see code) it can be tricky to get the
right placement - especially when working on very tight manuscripts.
If you want exact placement, add \verb!\usepackage{float}! to this file's header
and use [H] in the figure environment's placement options.


\marginpar{
	\vspace{0.8cm}
	\textbf{Figure \ref{fig2}. Example of a standard floating figure}.
	\textbf{A-F}, This figure is wrapped into the standard floating environment.
}
\begin{figure}[ht] %s state preferences regarding figure placement here

	% use to correct figure counter if necessary
	%\renewcommand{\thefigure}{2}

	\includegraphics[width=\textwidth]{figures/fig.pdf}


	\captionsetup{labelformat=empty} % makes sure dummy caption is blank
	\caption{}
	\label{fig2} % \label works only AFTER \caption within figure environment

\end{figure}

%\clearpage makes sure that all above content is printed at this point and does not invade into the upcoming content
%\clearpage

\section*{Discussion}
\subsection*{Subsection heading}

\lipsum[1-2]

\section*{Conclusion}

\lipsum[1-2]

\section*{Code and data availability}
The code to reproduce the simulations and the figures of this study
is available at \url{https://github.com/ismael-lajaaiti}.

\section*{Competing interest}
The authors declare no competing interest.


\section*{Acknowledgments}
We thank [insert name] for their helpful discussion and comments on the manuscript.

% \nolinenumbers

% Generate references.
\bibliography{references}

\end{document}
