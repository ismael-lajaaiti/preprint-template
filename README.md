# Preprint template

LaTeX template for preprint manuscripts.

Inspired from
[bioRxiv Overleaf's template](https://www.overleaf.com/latex/templates/arxiv-slash-biorxiv-template/phncddwqtxpc)
and [Poistlab's template](https://github.com/PoisotLab/manuscript-template).

The manuscript should be written in `main.tex` and,
if needed supplementary information should be written in `supplementary.tex`.
